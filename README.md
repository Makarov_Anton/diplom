# README #
This README would normally document whatever steps are necessary to get your application up and running.

# Установка #
* git clone https://Makarov_Anton@bitbucket.org/Makarov_Anton/diplom.git
* cd diplom

# Сборка: #
* Запустите миграцию при помощи команды php artisan migrate затем php artisan db:seed или установите бд из дампа diplom/diplom.sql

# Вход по умолчанию: #
* login: admin@admin.ru
* password: admin

# UML Схема #
* diplom /uml схема.xlsx

# Описание системы #
* https://docs.google.com/document/d/1riggeiI8913U0AeoOV3osNS14_PF8-UTQF1pHQmh-Ko/edit?usp=sharing
