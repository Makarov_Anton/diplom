-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.38 - MySQL Community Server (GPL)
-- Операционная система:         Win32
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных diplom
DROP DATABASE IF EXISTS `diplom`;
CREATE DATABASE IF NOT EXISTS `diplom` /*!40100 DEFAULT CHARACTER SET armscii8 */;
USE `diplom`;

-- Дамп структуры для таблица diplom.faqs
DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publish` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы diplom.faqs: ~41 rows (приблизительно)
DELETE FROM `faqs`;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` (`id`, `publish`, `status`, `question`, `answer`, `user_id`, `theme_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 1, 'Что значит, сверстать?', 'Это значит по нарисованному рисунку сайта (внешнему виду) создать компьютерный код отображения страницы сайта. \n 10 Нравится Комментировать', 1, 1, '2018-04-19 08:12:41', NULL),
	(2, 2, 1, 'Как вставлять комментарии в HTML?', '<!-- это комментарий -->\r\n<!-- это тоже,\r\nтолько занимает больше одной линии -->', 1, 1, '2018-04-19 08:19:33', '2018-04-19 08:20:56'),
	(3, 2, 1, 'Говорят, для поисковиков надо прописать ключевые слова. Что это?', 'Один из вариантов , определяющий ключевые слова или краткое описание документа. Некоторые поисковые роботы обращают на них внимание, а некоторые не обращают. Поисковый робот, знаете ли, тоже нам ничем не обязан.\r\n\r\nСлова - <meta name="Keywords" content="слово слово слово ...">\r\nОписание - <meta name="Descripton" content="описание документа">', 1, 1, '2018-04-19 08:21:22', '2018-04-19 08:22:33'),
	(4, 2, 1, 'В чем отличия <b> и <strong>, <i> и <em>?', 'Вообще говоря, никакой браузер не обязан вебмастеру показывать жирные или курсивные шрифты. Например, у консольного браузера их нет. Это уже элементы верстки, а HTML предназначен не для верстки, а для логической разметки.\r\n\r\n<b> и <i> - тэги физического выделения, то есть вы принудительно заставляете выделять текст каким-то видом шрифта. <strong> и <em> - тэги логического выделения. Каждый браузер может по-своему выделить текст внутри этих тэгов, так, как удобно его пользователю. <em> означает выделение, а <strong> означает усиленное выделение.\r\n\r\nРезюме: если вам нужно выделить текст, пользуйтесь <em>. Если вам нужно не выделить текст, а сделать его курсивом, пользуйтесь <i>.', 1, 1, '2018-04-19 08:21:44', '2018-04-19 08:22:38'),
	(5, 2, 1, 'Как "разлиновать" табличку тонкими линиями нужного цвета?', '<table border="0" cellspacing="0" cellpadding="0" bordercolor="...цвет\r\nлиний...">\r\n<tr>\r\n<td><table width="100%" cellspacing="...толщина линий..." border="0">\r\n<tr bgcolor="...цвет фона первой строки...">\r\n...первая строка...\r\n</tr>\r\n<tr bgcolor="...цвет фона второй строки...">\r\n...вторая строка...\r\n</tr>\r\n....\r\n</table></td>\r\n</tr>\r\n</table>', 1, 1, '2018-04-19 08:22:07', '2018-04-19 08:22:41'),
	(6, 2, 1, 'Я, к сожалению, употребил тег, который поддерживается только в браузере NN. Что же увидят пользователи других браузеров?', 'В стандарте можно прочитать, что пользовательские агенты должны игнорировать неизвестные им теги. Браузеры так и делают.', 1, 1, '2018-04-19 08:22:20', '2018-04-19 08:22:43'),
	(7, 2, 1, 'Чтобы был абзацный отступ?', 'P{margin-top:0;text-indent:5em;}', 1, 1, '2018-04-19 08:23:29', '2018-04-19 08:23:37'),
	(8, 2, 1, 'Как подавить подчеркивание ссылок в некоторых местах?', '<HEAD>\r\n<STYLE type="text/css"><!--\r\na.noneline {text-decoration: none;}\r\n--></STYLE>\r\n</HEAD>', 1, 1, '2018-04-19 08:25:04', '2018-04-19 08:25:04'),
	(9, 2, 1, 'Вопрос: Как мне узнать координаты курсора мышки после нажатия кнопочки?', '<script language="JavaScript">\r\n<!--\r\n  window.captureEvents(Event.CLICK);\r\n  window.onclick= displayCoords;\r\n  function displayCoords(e) {\r\n  alert("x: " + e.pageX + " y: " + e.pageY);  }\r\n// -->\r\n</script>', 1, 2, '2018-04-19 08:26:07', '2018-04-19 08:26:07'),
	(10, 2, 1, 'Как мне организовать предварительную загрузку изображений', 'Вот метод предварительной загрузки изображений из книги Стефана Коха "ВВЕДЕНИЕ В JAVASCRIPT ДЛЯ МАГА".', 1, 2, '2018-04-19 08:26:17', '2018-04-19 08:26:40'),
	(11, 2, 1, 'Есть ли в JavaScript функции для работы со строками, например, выделение любого символа из строки?', 'Могу предложить описание всех методов объекта String', 1, 2, '2018-04-19 08:26:56', '2018-04-19 08:26:56'),
	(12, 2, 1, 'Как по ссылке перенаправить посетителя на предыдущую страницу?', '<a href="javascript:history.back();">Назад</a>', 1, 2, '2018-04-19 08:27:09', '2018-04-19 08:27:09'),
	(13, 2, 1, 'Как узнать размеры окна браузера?', 'function getWindowWidth() \r\n{ \r\nif (document.all) return document.body.clientWidth; \r\nif (document.layers) return innerWidth; \r\nreturn 800 \r\n}\r\n\r\nfunction getWindowHeight() \r\n{ \r\nif (document.all) return document.body.clientHeight; \r\nif (document.layers) return innerHeight; \r\nreturn 800 \r\n}', 1, 2, '2018-04-19 08:27:22', '2018-04-19 08:27:29'),
	(14, 2, 1, 'Почему document.write() открывает новое окно вместо того, чтобы писать в текущем окне?', 'Как только документ завершает загрузку, вы не можете использовать document.write()', 1, 2, '2018-04-19 08:27:40', '2018-04-19 08:28:15'),
	(15, 2, 1, 'Есть ли в скрипте метод, который позволяет прослушивать midi или wav не на фоне, а по событию? Или надо на Яве писать класс, который методом play(file_name) будет это делать.', 'Вот решение этого вопроса:   Функцию Music() вызывайте по нужному вам событию.', 1, 2, '2018-04-19 08:28:00', '2018-04-19 08:28:16'),
	(16, 2, 1, 'Необходимо создание такой формы (ФИО клиента), чтобы при клике "ОК" вместо нее открывался некий купон с введенными данными (ФИО), порядковым номером + собственный текст.', 'Вот реализация такой формы: http://www.javaportal.ru/javascript/examples/loadFormFIO.html.', 1, 2, '2018-04-19 08:28:11', '2018-04-19 08:28:17'),
	(17, 2, 1, 'Что такое PHP?', '(перевод документации) "PHP, что означает \'PHP: Hypertext Preprocessor\' (\'PHP: Предварительный Обработчик Гипертекста\'), является внедряемым в HTML языком описания скриптов.\r\n\r\nМногое из его синтаксиса было позаимствовано из C, Java и Perl с добавлением некоторых уникальных, специфичных для PHP, особенностей.\r\n\r\nЦелью создания языка является предоставление web-разработчикам возможности быстрого создания динамически генерируемых страниц."', 1, 3, '2018-04-19 08:30:30', '2018-04-19 08:30:30'),
	(18, 2, 1, 'Где взять PHP?', 'Hа сайте производителя: http://www.php.net/downloads.php', 1, 3, '2018-04-19 08:30:43', '2018-04-19 08:30:43'),
	(19, 2, 1, 'Как расшифровывается \'PHP\'?', '\'PHP\' - это рекурсивный акроним (аналогия с \'Linux\' - \'Linux Is Not UniX\') словосочетания \'PHP: Hypertext Preprocessor\' (\'PHP: Предварительный Обработчик Гипертекста\'), что успешно отражает общую идеологию его функционирования.\r\n\r\nДо версии PHP 3.0 название было несколько иным (\'PHP/FI\') и расшифровывалось как \'Personal Home Page tools / Forms Interpreter\' (\'Утилиты для Домашней Страницы / Интерпретатор Форм\').', 1, 3, '2018-04-19 08:30:54', '2018-04-19 08:30:54'),
	(20, 2, 1, 'Почему с\'апложенные jpeg-файлы не отобpажаются? (бьются?) Веб-сервер - русский Апач', 'Все дело в том, что русский Апач по умолчанию производит перекодировку файлов.\r\nТак, символ с кодом 0х00 он заменяет на пробел (символ с кодом 0х20).\r\nДля борьбы с этим эффектом нужно добавить в конфигурационный файл Апача httpd.conf следующее:\r\n\r\n<Location>\r\n    CharsetRecodeMultipartForms Off\r\n</Location>', 1, 3, '2018-04-19 08:31:11', '2018-04-19 08:31:11'),
	(21, 2, 1, 'Как опpеделить валидность e-mail?', 'Функция, предложенная Maxim Matyukhin :\r\n\r\nfunction valid_mail ($email) {\r\n	if (eregi("^[_\\.0-9a-z-]+@([0-9a-z][0-9a-z-\\.]+)\\.([a-z]{2,3}$)",\r\n          $email, $check)) {\r\n		if (getmxrr($check[1] . "." . $check[2]) return "Valid";\r\n		else return "No MX for " . $check[1] . "." . $check[2];\r\n	}\r\n	else return "Badly formed address";\r\n}', 1, 3, '2018-04-19 08:31:24', '2018-04-19 08:32:19'),
	(22, 2, 1, 'Имеем MySQL. Добавляем запись с автоинкрементным уникальным индексом. Как узнать значение последнего индекса?', 'Есть замечательная функция mysql_insert_id ([идент. соединения]). Возвращает целое значение, которое и есть необходимый идентификатор, сгенерированный полем AUTO_INCREMENT в последем операторе INSERT.', 1, 3, '2018-04-19 08:31:41', '2018-04-19 08:32:19'),
	(23, 2, 1, 'Скрещивание PHP/Win32 и Apache/Win32', 'Предполагается, что вы уже установили PHP и Apache. Для внесения пущей ясности положим, что PHP установлен в каталоге \'C:\\HTTP\\php\', а Apache в \'C:\\HTTP\\apache\', причем Apache уже настроен и без лишней ругани отзывается на имя localhost, а необходимые изменения внесены в php.ini. Теперь определимся, каким образом PHP будет функционировать: в виде CGI-приложения [1] или как SAPI-модуль Apache [2] (разбор полетов на предмет различий см.)', 1, 3, '2018-04-19 08:32:01', '2018-04-19 08:32:20'),
	(24, 2, 1, 'Hастройка PHP в качестве модуля Apache.', 'Добавьте в файл C:\\HTTP\\apache\\conf\\httpd.conf строчки:\r\n\r\n      LoadModule php4_module c:/HTTP/php/sapi/php4apache.dll\r\n      AddType application/x-httpd-php .php\r\nТестирование\r\n\r\nТеперь перезапустите Apache. Создайте в каталоге C:\\HTTP\\apache\\htdocs файл test.php следующего содержания:\r\n\r\n<?php phpinfo(); ?>\r\nЗапустите браузер и перейдите на http://localhost/test.php. Если ваш браузер отобразил нечто табличное с большим количеством непонятной информации, примите поздравления. Если вместо этого ваши старания увенчались \'Error 500 - Internal Server Error\', попробуйте прочитать эту статью немного более внимательно.', 1, 3, '2018-04-19 08:32:16', '2018-04-19 08:32:21'),
	(25, 2, 1, 'Могу ли я разрабатывать и продавать плагины за деньги?', 'Да, конечно можете, никаких ограничений на данный момент по этому поводу нет. Помните, что monstra cms работает под лицензией GNU GENERAL PUBLIC LICENSE Version 3:\r\nhttp://monstra.org/about/license\r\nа это значит, что любой пользователь, которому вы продадите плагин, может размещать его как и на форуме, так и на файлообменниках или других ресурсах, единственное требование, которое он должен будет выполнить - это указывать Вас, как разработчика и контактную информацию, например Ваш e-mail или сайт.', 1, 4, '2018-04-19 08:34:11', '2018-04-19 08:38:31'),
	(26, 2, 1, 'Что такое bootstrap?', 'Это фреймворк из трёх языков HTML/CSS/JS. Благодаря большому функционалу верстать сайты становится легко и быстро, ну конечно если во всем разобраться.', 1, 4, '2018-04-19 08:34:47', '2018-04-19 08:38:32'),
	(27, 2, 1, 'Какое строение у фреймворка?', 'Bootstrap насчитывает не малое количество компонентов, которые помогают нам верстать быстрее (да где-то вы это уже слышали). Поэтому давайте посмотрим, что вошло в эти компоненты.', 1, 4, '2018-04-19 08:35:21', '2018-04-19 08:38:32'),
	(28, 2, 1, 'Как сделать адаптивные блоки по вертикал?', 'Вам нужен вот этот скрипт. По ссылке есть сам файл, примеры и прочее - http://masonry.desandro.com/', 1, 4, '2018-04-19 08:36:47', '2018-04-19 08:38:33'),
	(29, 2, 1, 'Как в адаптивном сайте с изменяемой высотой футера, прибить его к низу страницы', '<footer class="navbar-fixed-bottom"> .... </footer>', 1, 4, '2018-04-19 08:37:07', '2018-04-19 08:38:34'),
	(30, 2, 1, 'Для чего нужен отрицательный margin и положительный padding в bootstrap?', 'У каждой колонки слева и справа есть padding, равный 15px. Он нужен для генерации одинакового отступа между колонками. Однако в этом случае отступ слева у первой и отступ справа у последней колонки в строке складывается с отступами контейнера.\r\n\r\nЧтобы нивелировать его используется .row с отрицательным margin равным 15px. Если использовать .row, то левый отступ у первого столбца и правый отступ у правого столбца "проваливаются" в уже существующие отступы у контейнера и не занимают дополнительного места.', 1, 4, '2018-04-19 08:37:40', '2018-04-19 08:38:34'),
	(31, 2, 1, 'Размер контейнера, равный максимальному эл-ту среди других детей (Bootstrap 3, CSS)', '2\r\nголос «против»\r\nпринят\r\n+50\r\nДело в том что и в таблице и во flex контейнере, и в большинстве других контейнеров ширина одних элементов зависит от ширины других. Это значит что если мы добавим 4 дочерних элемента, то контейнер рассчитывает параметры (ширину и высоту) каждого на основе всех остальных. Теперь если мы сделаем у всех размеры максимального элемента, то весь расчёт который сделала таблица или flex собьётся, и это уже будет не то что подразумевает flex контейнер или таблица. Например рассчитав максимальное и сделав все элементы под его размер у нас элементы могут вылезти за пределы границы, или перейти на следующую строчку в зависимости от настроек flex.', 1, 4, '2018-04-19 08:38:09', '2018-04-19 08:38:35'),
	(32, 2, 1, 'Bootstrap не работает span', 'span был в bootstrap 2, в bootstrap 3 сетку основательно переделали. Классы разметки, отвечающие за ширину колонок, .span2, .span3 и т.д. заменены на соответствующие .col-md-2, .col-md-3…', 1, 4, '2018-04-19 08:38:27', '2018-04-19 08:38:35'),
	(33, 2, 1, 'Как установить начальное значение для поля с auto increment в MySQL?', 'Если нужно при создании таблицы:\r\n\r\nCREATE TABLE tab (id INT UNSIGNED AUTO_INCREMENT,....PRIMARY KEY(id)) AUTO_INCREMENT=123;', 1, 5, '2018-04-19 08:39:30', '2018-04-19 08:41:27'),
	(34, 2, 1, 'Что такое DDL, DML and DCL?', 'Как вы видете существует большое разнообразие SQL комманд. Для удобства работы их можно разделить на 3 большие подгруппы. Data Definition Language (DDL) это команды, которые имеют дело с схемами БД и описанием того как данные должны размещаться в БД. Такие выражения языка как CREATE TABLE или ALTER TABLE принадлежат DDL. Data manipulation language (DML) имеет дело с манипуляцией данными и включает наиболее общие выражения языка, такие как SELECT, INSERT, UPDATE и т.п. Data Control Language (DCL) включает такие команды как GRANT, которые в основном касаются прав, разрешений и другого контроля над БД.', 1, 5, '2018-04-19 08:39:52', '2018-04-19 08:41:27'),
	(35, 2, 1, 'Как бы вы получили количество строк выдаваемых запросом ?', 'Запрос \r\nSELECT COUNT (user_id) FROM users \r\nвернет только количество user_id-ов.', 1, 5, '2018-04-19 08:40:03', '2018-04-19 08:41:28'),
	(36, 2, 1, 'Если значение колонки повторяется, как вы найдете уникальные значения ?', 'Использование DISTINCT в запросе вам поможет. Тогда запрос будет таким:\r\nSELECT DISTINCT user_firstname FROM users; \r\nВы можете также найти количество уникальных значений в колонке таким запросом:\r\nSELECT COUNT (DISTINCT user_firstname) FROM users;', 1, 5, '2018-04-19 08:40:28', '2018-04-19 08:41:29'),
	(37, 2, 1, 'Как вернуть сотню книг начиная с 25-ой ?', 'SELECT book_title FROM books LIMIT 25, 100 \r\nПервое значение команды LIMIT это смещение, а второе количество.', 1, 5, '2018-04-19 08:40:36', '2018-04-19 08:41:30'),
	(38, 2, 1, 'Как бы вы написали запрос выборки всех команд выигравших 2, 4, 6 или 8 игр ?', 'SELECT user_name FROM users WHERE ISNULL(user_phonenumber)', 1, 5, '2018-04-19 08:40:49', '2018-04-19 08:41:30'),
	(39, 2, 1, 'Что значит следующий запрос: SELECT user_name, user_isp FROM users LEFT JOIN isps USING (user_id) ?', 'SELECT user_name FROM users WHERE ISNULL(user_phonenumber)', 1, 5, '2018-04-19 08:41:02', '2018-04-19 08:41:31'),
	(40, 2, 1, 'Как узнать какой авто инкремент был использован при последнем INSERT-е ?', 'SELECT LAST_INSERT_ID() \r\n- запрос вернет значение назначенное функции auto_increment. Помните, что вы не должны указывать имя таблицы.', 1, 5, '2018-04-19 08:41:17', '2018-04-19 08:41:31'),
	(41, 2, 1, 'b или strong?', 'strong устаревший тэг, используйте b', 2, 1, '2018-04-19 08:49:46', '2018-04-19 08:50:46');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;

-- Дамп структуры для таблица diplom.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы diplom.migrations: ~4 rows (приблизительно)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_04_06_072353_craete_faqs_table', 1),
	(4, '2018_04_06_072602_craete_themes_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица diplom.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы diplom.password_resets: ~0 rows (приблизительно)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп структуры для таблица diplom.themes
DROP TABLE IF EXISTS `themes`;
CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы diplom.themes: ~5 rows (приблизительно)
DELETE FROM `themes`;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` (`id`, `title`, `created_at`, `updated_at`) VALUES
	(1, 'HTML', '2018-04-19 08:12:41', '2018-04-19 08:16:43'),
	(2, 'JS', '2018-04-19 08:13:01', '2018-04-19 08:13:01'),
	(3, 'PHP', '2018-04-19 08:17:48', '2018-04-19 08:17:48'),
	(4, 'BOOTSTRAP', '2018-04-19 08:18:12', '2018-04-19 08:18:12'),
	(5, 'MYSQL', '2018-04-19 08:18:27', '2018-04-19 08:18:27');
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;

-- Дамп структуры для таблица diplom.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы diplom.users: ~2 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@admin.ru', '$2y$10$6qlIrd3WcEiG78nkELquuu0NYXG6oAiInfQU9IjBNXf15RqaeHwWS', 'fqaKSg1F5kGOer5TwODjMkDXtAhTwdIN5OM5aUssWDsHMFfi3ERd735AyfXt', '2018-04-19 08:12:41', NULL),
	(2, 'Антон', 'anton@mail.ru', '', NULL, '2018-04-19 08:49:46', '2018-04-19 08:49:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
