<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?fafacemily=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!-- Styles -->

    <link rel="stylesheet" href="{{ asset('css/reset.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}"> <!-- Resource style -->
    <script src="js/modernizr.js"></script> <!-- Modernizr -->
    <style>
        .first {
            list-style: none;
            padding: 0;
        }

        .first li {
            padding: 10px 30px;
            background: linear-gradient(to left, lightgreen 0%, white, lightgreen);
            border-bottom: 1px solid grey;
            color: #506a6b;
            font-size: 20px;
            box-shadow: 0 5px 5px 0 rgba(0, 0, 0, .2);
            margin-bottom: 5px;
        }

        .first li:last-child {
            border-bottom: none;
        }
    </style>
</head>
<body>
<header>
    <h1>FAQ</h1>
</header>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            @if (Route::has('login'))
                <div class="links">

                    @auth
                        <div style="text-align: right">
                            <div class="first" style="margin: 20px 0;width: 300px;">
                                <ul>
                                    <li>
                                        <a class="" href="{{ url('/home') }}">Home</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @else
                        <div style="text-align: right">
                            <div class="first" style="margin: 20px 0;width: 300px;">
                                <ul>
                                    <li>
                                        <a href="{{route('guests.create')}}">New question</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('login') }}">Login</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('register') }}">Register</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endauth

                </div>
            @endif
        </div>

        <div class="row">
            @yield('content')
        </div>
    </div>
</div>
<script src="js/jquery-2.1.1.js"></script>
<script src="js/jquery.mobile.custom.min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
