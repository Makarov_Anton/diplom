@extends('index')

@section('title', 'Главная страница')

@section('content')
    <section class="cd-faq guest">
        <ul class="cd-faq-categories">
            @foreach($themes as $theme)
                <li><a href="#{{ $theme->title }}">{{ $theme->title }}</a></li>
            @endforeach
        </ul> <!-- cd-faq-categories -->
        <div class="cd-faq-items">
            @foreach($themes as $theme)<ul id="{{ $theme->title }}" class="cd-faq-group"><li class="cd-faq-title"><h2>{{ $theme->title }}</h2></li>
                @foreach($theme->faqs as $faq)
                    @if ($faq->status == '1' )
                        <li>
                            <a class="cd-faq-trigger" href="#0">{{ $faq->question }}</a>
                            <div class="cd-faq-content">
                                <p>{{ $faq->answer }}</p>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
            @endforeach
        </div>
    </section>
@endsection