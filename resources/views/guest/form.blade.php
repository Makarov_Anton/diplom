@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
						<div class="panel-heading">Your question</div>
					@if($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="panel-body">
						<form action="{{ route('guests.store') }}" method="POST">
							<div class="row">
								{{ csrf_field() }}
								@include('admin.fields.anythingGuest', ['type' => 'text', 'field' => 'name', 'name' => 'Name'])
								@include('admin.fields.anythingGuest', ['type' => 'email', 'field' => 'email', 'name' => 'E-Mail'])
								@include('admin.fields.textarea', ['field' => 'question', 'name' => 'Question', 'rows' => 2])
								@include('admin.fields.select', ['field' => 'theme_id', 'name' => 'Theme', 'options' => $themes])
							</div>
							<input type="submit" value="Register">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection