@extends('admin.fields.main')

@section('field')
	<input type="{{$type}}" name="{{ $field }}" value="{{ old($field, (isset($entity) ? $entity->$field : '')) }}" class="form-control" placeholder="{{  $name }}">
@overwrite
