@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					@if(empty($entity))
						<div class="panel-heading">Create new user</div>
					@else
						<div class="panel-heading">Edit user</div>
					@endif

					@if($errors->any())
						<div class="alert alert-danger">
							<ul>
								@foreach($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="panel-body">
						<form action="@if(empty($entity)){{ route('users.store') }}@else{{ route('users.update', $entity->id) }}@endif" method="POST">
							{{ csrf_field() }}
							@isset($entity)
								 {{ method_field('PUT') }}
							@endisset
							<div class="row">
								{{ csrf_field() }}
								@include('admin.fields.anything', ['type' => 'text', 'field' => 'name', 'name' => 'Name'])
								@include('admin.fields.anything', ['type' => 'email', 'field' => 'email', 'name' => 'E-Mail'])
								@include('admin.fields.password', ['type' => 'password', 'field' => 'password', 'name' => 'Password'])
								@include('admin.fields.password', ['type' => 'password', 'field' => 'password_confirmation', 'name' => 'Confirm Password'])
							</div>
							<input type="submit" value="Register">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection