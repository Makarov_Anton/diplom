@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<h2 class="panel-heading">Administrators</h2>
					<div class="text-center">
						<div class="col-md-6 " style="margin-bottom: 10px"><a type="button" class="btn btn-success" href="{{ route('users.create') }}">Create new administrator</a></div>
					</div>
					<div class="panel-body">
						@if($users->count() > 0)
							<table class="table">
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Mail</th>
									<th>Actions</th>
								</tr>
								@foreach($users as $user)
									<tr>
										<td>{{ $user->id }}</td>
										<td>{{ $user->name }}</td>
										<td>{{ $user->email }}</td>
										<td>
											<form action="{{ route('users.destroy', $user->id) }}" method="POST">
												<a type="button" class="btn btn-default" href="{{ route('users.edit', $user->id) }}">edit</a>
												{{ method_field('DELETE') }}
												{{ csrf_field() }}
												<button type="submit" class="btn btn-danger">delete</button>
											</form>
										</td>
									</tr>
								@endforeach
							</table>
							<div class="text-center">
								<a type="button" class="btn btn-primary" href="{{ route('themes.index') }}">Main menu</a>
							</div>
							<div class="text-center">{{$users->render()}}</div>
						@else
							No users
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection