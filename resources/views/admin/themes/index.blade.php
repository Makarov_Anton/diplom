@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <h2 class="panel-heading">Main menu</h2>
                    <div class="text-center">
                        <div class="col-md-6 " style="margin-bottom: 10px"><a type="button" class="btn btn-success"
                                                                              href="{{ route('faqs.create') }}">Creat
                                new question</a></div>
                        <div class="col-md-6" style="margin-bottom: 10px"><a type="button" class="btn btn-success"
                                                                             href="{{ route('themes.create') }}">Creat
                                new themes</a></div>
                    </div>
                    <div class="panel-body">
                        @if($themes->count() > 0)
                            <table class="table">
                                <tr>
                                    <th>ID</th>
                                    <th>Theme</th>
                                    <th>Count</th>
                                    <th>Publish</th>
                                    <th>No answer</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($themes as $theme)
                                    <tr>
                                        <td>{{ $theme->id }}</td>
                                        <td>
                                            <a type="button" class="btn btn-primary"
                                               href="{{ route('faqs.index.link', $theme->id) }}">{{ $theme->title }}</a>
                                        </td>
                                        <td>{{ $theme->faqs->count() }}</td>
                                        <td>{{ $theme->faqs->where('publish', '2')->count() }}</td>
                                        <td>{{ $theme->faqs->where('status', '0')->count() }}</td>
                                        <td>
                                            <form action="{{ route('themes.destroy', $theme->id) }}" method="POST">
                                                <a type="button" class="btn btn-default"
                                                   href="{{ route('themes.edit', $theme->id) }}">edit</a>
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger">delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="text-center">
                                <a type="button" class="btn btn-primary"
                                   href="{{ route('faqs.index.link', 'noanswer') }}">All question without answer</a>
                            </div>
                            <div class="text-center">{{$themes->render()}}</div>
                        @else
                            No themes
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection