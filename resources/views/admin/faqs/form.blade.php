@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					@if(empty($entity))
						<div class="panel-heading">Create new faq</div>
					@else
						<div class="panel-heading">Edit faq</div>
					@endif
					<div class="panel-body">
						<form action="@if(empty($entity)){{ route('faqs.store') }}@else{{ route('faqs.update', $entity->id) }}@endif" method="POST">
							{{ csrf_field() }}
							@isset($entity)
								{{ method_field('PUT') }}
							@endisset
							<div class="row">
                                @if(!empty($entity))
                                	@include('admin.fields.text', ['field' => 'name', 'name' => 'author', 'entity' => $entity->user->name])
                                @endif
                                @include('admin.fields.textarea', ['field' => 'question', 'name' => 'Question', 'rows' => 2])
                                @include('admin.fields.textarea', ['field' => 'answer', 'name' => 'Answer', 'rows' => 5])
                                @include('admin.fields.select', ['field' => 'theme_id', 'name' => 'Theme', 'options' => $themes])
							</div>
							<p class="text-center"><input type="checkbox" name="publish" value="2">Click if your want to publish?</p>
							<input type="submit" class="btn btn-info" value="save">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection


