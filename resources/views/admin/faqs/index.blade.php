@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<h2 class="panel-heading">FAQ\{{isset($link) ? $link : '' }}</h2>
					<div class="panel-body">
						@if($faqs->count() > 0)
							<table class="table text-center">
								<tr>
									<th>Date create</th>
									<th class="text-center">Question</th>
									<th class="text-center">Status</th>
									<th class="text-center">Author</th>
									<th class="text-center">Condition</th>
									<th class="text-center">Actions</th>
								</tr>
								@foreach($faqs as $faq)

									<tr>
										<td>{{ $faq->created_at }}</td>
										<td class="text-left">{{(mb_strlen($faq->question) > 40) ? mb_substr($faq->question, 0, 40) . "..." : mb_substr($faq->question, 0, 40)}}</td>
										@if($faq->status == 0)
											<td>no answer</td>
										@else
											<td>answer</td>
										@endif

										<td>{{$faq->user->name}}</td>
										<td>
											<form action="{{ route('faqs.publish') }}" method="POST">
												{{ csrf_field() }}
												<input type="hidden" name="id" value="{{$faq->id}}">
												<input type="hidden" name="publish" value="{{$faq->publish}}">
													<button type="submit" name='publish' value="0" class="btn {{($faq->publish == '0') ? "btn-info" : "btn-default"}}">hidden</button>
													<button type="submit" name='publish' value="1" class="btn {{($faq->publish == '1') ? "btn-warning" : "btn-default"}}">expect an answer</button>
													<button type="submit" name='publish' value="2" class="btn {{($faq->publish == '2') ? "btn-success" : "btn-default"}}">publish</button>
											</form>
										</td>
										<td>
											<form action="{{ route('faqs.destroy', $faq->id) }}" method="POST">
												<a type="button" class="btn btn-warning" href="{{ route('faqs.edit', $faq->id) }}">edit</a>
												<input type="hidden" name="link" value="{{$link}}">
												{{ method_field('DELETE') }}
												{{ csrf_field() }}
												<button type="submit" class="btn btn-danger">delete</button>
											</form>
										</td>
									</tr>
								@endforeach
							</table>
							<div class="text-center">
								<a type="button" class="btn btn-primary" href="{{ route('themes.index') }}">Main menu</a>
							</div>
							<div class="text-center">{{$faqs->render()}}</div>
						@else
							No faqs
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection