<?php

Auth::routes();

Route::group(['middleware' => 'auth'], function (){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('faqs', 'FaqsController');
    Route::post('/faqs/publish', 'FaqsController@publish')->name('faqs.publish');
    Route::get('/faqs/index/{link}', 'FaqsController@link')->name('faqs.index.link');
    Route::resource('themes', 'ThemesController');
    Route::resource('users', 'UsersController');
});

Route::resource('guests', 'GuestsController');
Route::get('/', 'PagesController@index')->name('pages.index');
