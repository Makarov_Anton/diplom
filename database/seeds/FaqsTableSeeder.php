<?php

use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('faqs')->insert([
            'question' => 'Что значит, сверстать?',
            'user_id' => 1,
            'theme_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'status' => 1,
            'answer' => 'Это значит по нарисованному рисунку сайта (внешнему виду) создать компьютерный код отображения страницы сайта. 
 10 Нравится Комментировать',
            'publish' => 2
        ]);
    }
}

