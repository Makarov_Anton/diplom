<?php

use Illuminate\Database\Seeder;

class ThemesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('themes')->insert([
            'title' => 'html',
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
