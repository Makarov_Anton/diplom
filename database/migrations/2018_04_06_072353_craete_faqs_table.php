<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CraeteFaqsTable extends Migration
{
    /**publish:
     * 0 - hidden
     * 1 - expect an answer
     * 2 - publish
     *
     * answer:
     * 0 - noanswer
     * 1 - answer
     *
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('publish')->default('0');
            $table->integer('status')->default('0');
            $table->text('question');
            $table->text('answer')->nullable();
            $table->integer('user_id');
            $table->integer('theme_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
