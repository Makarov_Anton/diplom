<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{

    protected $fillable = [
        'status',
        'publish',
        'question',
        'answer',
        'user_id',
        'theme_id'
    ];

    public function scopeNoanswer($query)
    {
        return $query->where('status', '0');
    }

    public function scopeLink($query, $link)
    {
        return $query->where('theme_id', $link);
    }

    public function theme()
    {
        return $this->belongsTo('App\Theme');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
