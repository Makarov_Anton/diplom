<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $fillable = [
        'title',
    ];

    public function faqs()
    {
        return $this->hasMany('App\faq');
    }
}
