<?php

namespace App\Http\Controllers;

use App\Theme;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $themes = Theme::paginate(10);
        return view('admin.themes.index', compact('themes'));
    }
}
