<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Theme;


class PagesController extends Controller
{
    public function index()
    {

        $themes = Theme::all();
        return view('guest.index', compact('themes'));
    }
}
