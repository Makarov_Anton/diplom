<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Theme;
use App\User;
use Illuminate\Http\Request;

class GuestsController extends Controller
{

    public function create()
    {
        $themes = Theme::all();
        return view('guest.form', compact('themes'));
    }

    public function store(Request $request)
    {

        $validator = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users'
        ]);
        $pass = "";
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => $pass
        ]);

        $user_id = User::where('name', $request['name'])->value('id');

        $this->validate($request, [
            'question' => 'required',
            'theme_id' => 'required|integer',
        ]);

        Faq::create([
            'theme_id' => $request->theme_id,
            'question' => $request->question,
            'user_id' => $user_id
        ]);

        return redirect()->route('pages.index');
    }

}
