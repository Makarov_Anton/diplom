<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Theme;
use Illuminate\Http\Request;

class ThemesController extends Controller
{
    public function index()
    {
        $themes = Theme::paginate(10);
        return view('admin.themes.index', compact('themes'));
    }

    public function create()
    {
        return view('admin.themes.form');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        Theme::create($request->all());
        return redirect()->route('themes.index');
    }

    public function edit(Theme $theme)
    {
        $entity = $theme;
        return view('admin.themes.form', compact('entity'));
    }

    public function update(Request $request, Theme $theme)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
        $theme->update($request->all());

        return redirect()->route('themes.index');
    }


    public function destroy(Theme $theme)
    {
        $theme->delete();
        return redirect()->route('themes.index');
    }

}
