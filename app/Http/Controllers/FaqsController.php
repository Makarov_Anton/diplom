<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Faq;
use App\Theme;
use App\User;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    public function index()
    {
        $faqs = Faq::paginate(10);
        return view('admin.faqs.index', compact('faqs'));
    }

    public function create()
    {
        $themes = Theme::all();
        return view('admin.faqs.form', compact('themes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'theme_id' => 'required|integer',
        ]);
        $data = $request->all();
        $data['status'] = ($request->has('answer') && $data['answer'] !== null) ? 1 : 0;
        $data['user_id'] = Auth::user()->id;
        Faq::create($data);
        return redirect()->route('themes.index');
    }

    public function edit(faq $faq)
    {
        $entity = $faq;
        $themes = Theme::all();
        return view('admin.faqs.form', compact('themes', 'entity'));
    }

    public function update(Request $request, Faq $faq)
    {
        $this->validate($request, [
            'name' => 'required',
            'question' => 'required',
            'theme_id' => 'required|integer'
        ]);
        $data = $request->all();
        $data['status'] = ($request->has('answer') && $data['answer'] !== null) ? 1 : 0;
        User::find($faq->user_id)->update(['name' => $request->name]);
        $faq->update($data);
        return redirect()->route('themes.index');
    }


    public function destroy(Faq $faq)
    {
        $faq->delete();
        return redirect()->back();
    }

    public function publish(Request $request)
    {
        Faq::find($request->id)->update(['publish' => $request->publish]);
        return redirect()->back();
    }

    public function link($link)
    {

        if ($link == 'noanswer') {
            $link = 'noanswer';
            $faqs = Faq::noanswer()->paginate(10);
        }elseif($link == 'login'){
            return redirect()->route('themes.index');
        }else{
            $faqs = Faq::link($link)->paginate(10);
            $link = Theme::find($link)->value('title');
        }

        return view('admin.faqs.index', compact('faqs', 'link'));
    }

}
